
# Building and running
### Compiling program
Go to  main project path (where`CMakeLists.txt` is located) and type:
```commandLine
cmake .
make
```

### Running program
```commandLine
sudo ./bin/dns_spoofing PATH_TO_CONFIG_FILE
```
or with example config file:

```commandLine
sudo ./bin/dns_spoofing app.config
```
Note that this program requires root privileges.

### Testing
```commandline
dig -q www.wp.pl
```
### Config file
Parameters:

Required: 
* DEVICE_NAME - device name used to capture and to send packets
* GATEWAY_ADDRESS - gateway address in your network
* VERBOSE - logging setup

Then you can type domains to spoof in format (with space bettwen domain name and ip): DOMAIN_NAME FAKE_IP
```commandLine
DEVICE_NAME wlp2s0
GATEWAY_ADDRESS 192.168.0.1
VERBOSE false
www.facebook.pl 150.254.30.29
www.wp.pl 213.180.141.140 
```

