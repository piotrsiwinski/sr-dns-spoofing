//
// Created by root on 01.06.18.
//

#ifndef DNS_SPOOFING_LOG_UTIL_H
#define DNS_SPOOFING_LOG_UTIL_H


#include <sys/types.h>
#include <cstdio>
#include <netinet/ip.h>
#include <netinet/udp.h>

bool verbose = false;

class log_util {
public:
    static void init_logger(bool is_verbose) {
        verbose = is_verbose;
    }

    static void log(string message){
        cout << message << endl;
    }

    static void log_debug(string message){
        if (!verbose) {
            return;
        }
        log(message);
    }

    static void log_ip_header(const iphdr *iph) {
        if (!verbose) {
            return;
        }
        struct sockaddr_in source, dest;
        memset(&source, 0, sizeof(source));
        source.sin_addr.s_addr = iph->saddr;

        memset(&dest, 0, sizeof(dest));
        dest.sin_addr.s_addr = iph->daddr;
        printf("\n");
        printf("\n\n***********************IP Header*************************\n");
        printf("   |-IP Version        : %d\n", iph->version);
        printf("   |-IP Header Length  : %d DWORDS or %d Bytes\n", iph->ihl,
               iph->ihl * 4);
        printf("   |-Type Of Service   : %d\n", (unsigned int) iph->tos);
        printf("   |-IP Total Length   : %d  Bytes(Size of Packet)\n", ntohs(iph->tot_len));
        printf("   |-Identification    : %d\n", ntohs(iph->id));
        //printf("   |-Reserved ZERO Field   : %d\n",(unsigned int)iphdr->ip_reserved_zero);
        //printf("   |-Dont Fragment Field   : %d\n",(unsigned int)iphdr->ip_dont_fragment);
        //printf("   |-More Fragment Field   : %d\n",(unsigned int)iphdr->ip_more_fragment);
        printf("   |-TTL      : %d\n", (unsigned int) iph->ttl);
        printf("   |-Protocol : %d\n", (unsigned int) iph->protocol);
        printf("   |-Checksum : %d\n", ntohs(iph->check));
        printf("   |-Source IP        : %s\n", inet_ntoa(source.sin_addr));
        printf("   |-Destination IP   : %s\n", inet_ntoa(dest.sin_addr));
    }

    void static log_ethernet_header(const struct ethhdr *eth) {
        if (!verbose) {
            return;
        }
        printf("\n");
        printf("\n\n*********************** ETHERNET FRAME *************************\n");
        printf("   |-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_dest[0], eth->h_dest[1],
               eth->h_dest[2], eth->h_dest[3], eth->h_dest[4], eth->h_dest[5]);
        printf("   |-Source Address      : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_source[0], eth->h_source[1],
               eth->h_source[2], eth->h_source[3], eth->h_source[4], eth->h_source[5]);
        printf("   |-Protocol            : %u \n", eth->h_proto);
    }


    void static log_udp_details(const u_char *Buffer, int Size, unsigned short iphdrlen, const udphdr *udph,
                                int header_size) {
        //todo: remove unused method params
        if (!verbose) {
            return;
        }
        printf("\n\n***********************UDP Packet*************************\n");

        printf("\nUDP Header\n");
        printf("   |-Source Port      : %d\n", ntohs(udph->source));
        printf("   |-Destination Port : %d\n", ntohs(udph->dest));
        printf("   |-UDP Length       : %d\n", ntohs(udph->len));
        printf("   |-UDP Checksum     : %d\n", ntohs(udph->check));

        printf("\nData Payload\n");
    }

    void static log_dns_header(const dns_hdr *dns_header) {
        if (!verbose) {
            return;
        }
        printf("\n\n*********************** DNS Header *************************\n");
        printf("\nThe response contains: ");
        printf("\nID: %d", ntohs(dns_header->id));
        printf("\nQuestions number: %d  ", ntohs(dns_header->q_count));
        printf("\nAnswers number: %d", ntohs(dns_header->ans_count));
    }

};


#endif //DNS_SPOOFING_LOG_UTIL_H
