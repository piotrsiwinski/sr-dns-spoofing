//
// Created by piotr on 02.05.18.
//

#ifndef DNS_SPOOFING_SPOOFINGSERVICE_H
#define DNS_SPOOFING_SPOOFINGSERVICE_H

#include <string>
#include <libnet.h>

using namespace std;

class SpoofingService {
private:
    libnet_t *ln;
public:
    SpoofingService(string s1, string s2);

    void send_arp_reply();
};


#endif //DNS_SPOOFING_SPOOFINGSERVICE_H
