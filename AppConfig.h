//
// Created by root on 01.06.18.
//

#ifndef DNS_SPOOFING_APPCONFIG_H
#define DNS_SPOOFING_APPCONFIG_H


#include <map>
#include <fstream>
#include <iostream>

using namespace std;

class AppConfig {
private:
    const string DEVICE_NAME = "DEVICE_NAME";
    const string GATEWAY_ADDRESS = "GATEWAY_ADDRESS";
    const string VERBOSE = "VERBOSE";
    std::map<string, string> settings;

public:
    void loadConfig(string path) {
        std::ifstream file(path);
        std::string key, value;

        while (file >> key >> value) {
            this->settings.insert(std::pair<std::string, std::string>(key, value));
        }
    }

    string get_device_name() {
        if (this->settings.find(DEVICE_NAME) == this->settings.end()) {
            return nullptr;
        };
        return this->settings.find(DEVICE_NAME)->second;
    }

    string get_gateway_address() {
        if (this->settings.find(GATEWAY_ADDRESS) == this->settings.end()) {
            return nullptr;
        };
        return this->settings.find(GATEWAY_ADDRESS)->second;
    }

    bool isOnBlackList(string domain) {
        if (this->settings.find(domain) == this->settings.end()) {
            return false;
        };
        return true;
    }

    bool isVerbose(){
        if (this->settings.find(VERBOSE) == this->settings.end()) {
            return false;
        };

        return this->settings.find(VERBOSE)->second == "true" || this->settings.find(VERBOSE)->second == "TRUE";
    }

    string get_fake_ip_by_domain_name(string domain_name) {
        if (this->settings.find(domain_name) == this->settings.end()) {
            return nullptr;
        };
        return this->settings.find(domain_name)->second;
    }

};

#endif //DNS_SPOOFING_APPCONFIG_H
