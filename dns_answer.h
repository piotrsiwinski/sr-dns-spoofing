//
// Created by root on 12.06.18.
//
#include <pcap.h>
#include <string>
#include <cstdio>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <libnet.h>

#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <sstream>
#include <iomanip>
#include <vector>
#include "dns_hdr.h"
#include "AppConfig.h"


using namespace std;
#ifndef DNS_SPOOFING_DNS_ANSWER_H
#define DNS_SPOOFING_DNS_ANSWER_H
struct dns_answer {
    dns_answer(const string &ip_address) :
            name(htons(0xc00c)),
            type(htons(1)),
            dns_class(htons(1)),
            ttl1(htons(0x0)),
            ttl2(htons(0x4e)),
            data_length(htons(4)) {
        std::stringstream stream, stream2;

        istringstream iss(ip_address);
        std::vector<int> tokens;
        std::string token;
        while (std::getline(iss, token, '.')) {
            if (!token.empty())
                tokens.push_back(std::stoi(token));
        }

        stream << "0x" << std::setfill('0') << std::setw(sizeof(char) * 2) <<
               std::hex << tokens[0] << tokens[1];
        stream2 << "0x" << std::setfill('0') << std::setw(sizeof(char) * 2) <<
                std::hex << tokens[2] << tokens[3];

        int number1 = (int) strtol(stream.str().c_str(), nullptr, 0);
        int number2 = (int) strtol(stream2.str().c_str(), nullptr, 0);
        address1 = htons(number1);
        address2 = htons(number2);
    }

    __be16 name;
    __be16 type;
    __be16 dns_class;
    __be16 ttl1;
    __be16 ttl2;
    __be16 data_length;
    __be16 address1;
    __be16 address2;
};

struct QUESTION {
    __be16 qtype;
    __be16 qclass;
};
#endif //DNS_SPOOFING_DNS_ANSWER_H
